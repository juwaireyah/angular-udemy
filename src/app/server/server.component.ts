import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
 // styleUrls: ['./server.component.css']
  styles:[`
    .online{
      color:white;
    }`
  ]
})
export class ServerComponent implements OnInit {
  serverId: number = 10;
  serverStatus: string  = 'offline';
  
  getServerStatus(){
    return this.serverStatus;
  }
  constructor() { 
    /*if random is more than 0.5, set serverStatus to onlie. Otherwise, set serverStatus to offline*/
    this.serverStatus = Math.random() > 0.5 ? 'online' :'offline';
  }

  ngOnInit() {
  }

  getColor(){
    /*if serverStatus is "online", return green. Otherwise return red */
    return this.serverStatus === 'online' ? 'green' : 'red';
  }
}
